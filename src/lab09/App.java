package lab09;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileSystemView;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class App {

	private JFrame frame;
	
	private JRadioButton rdbtnDecode;
	
	private File inputFile;
	private JTextField textFieldKey;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("WERSJA 2.0");
		frame.setBounds(100, 100, 277, 217);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnWskaPlik = new JButton("Wska\u017C plik");
		btnWskaPlik.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

				int returnValue = jfc.showOpenDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					inputFile = jfc.getSelectedFile();
				}
			}
		});
		btnWskaPlik.setBounds(65, 33, 118, 23);
		frame.getContentPane().add(btnWskaPlik);
		
		JRadioButton rdbtnCode = new JRadioButton("Zakoduj");
		rdbtnCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean selected = rdbtnCode.isSelected();
				if(selected) {
					rdbtnDecode.setSelected(false);
				}
			}
		});
		rdbtnCode.setBounds(24, 98, 109, 23);
		frame.getContentPane().add(rdbtnCode);
		
		rdbtnDecode = new JRadioButton("Dekoduj");
		rdbtnDecode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean selected = rdbtnDecode.isSelected();
				if(selected) {
					rdbtnCode.setSelected(false);
				}
			}
		});
		rdbtnDecode.setBounds(146, 98, 109, 23);
		frame.getContentPane().add(rdbtnDecode);
		
		JButton btnInput = new JButton("Wykonaj");
		btnInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textFieldKey.getText().length() != 16) {
					JOptionPane.showMessageDialog(null, "Niepoprawna d�ugo�� klucza. Podaj 16 znak�w");
					return;
				}
				
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

				int mode;
				if(rdbtnCode.isSelected())
					mode = Cipher.ENCRYPT_MODE;
				else
					mode = Cipher.DECRYPT_MODE;
				
				int returnValue = jfc.showSaveDialog(null);

				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File outputFile = jfc.getSelectedFile();
					int status = doCrypto(mode, textFieldKey.getText(), inputFile, outputFile);
					if(status != 0)
						JOptionPane.showMessageDialog(null, "Szyfrowanie nie powiod�o si�!");
				}
			}
		});
		btnInput.setBounds(83, 144, 89, 23);
		frame.getContentPane().add(btnInput);
		
		textFieldKey = new JTextField();
		textFieldKey.setBounds(40, 71, 172, 20);
		frame.getContentPane().add(textFieldKey);
		textFieldKey.setColumns(10);
	}
	
	 private static int doCrypto(int cipherMode, String key, File inputFile,
	            File outputFile) {
	            try {
	       		 	Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
		            Cipher cipher = Cipher.getInstance("AES");
		            cipher.init(cipherMode, secretKey);
		             
		            FileInputStream inputStream = new FileInputStream(inputFile);
		            byte[] inputBytes = new byte[(int) inputFile.length()];
		            inputStream.read(inputBytes);
		             
		            byte[] outputBytes = cipher.doFinal(inputBytes);
		            FileOutputStream outputStream = new FileOutputStream(outputFile);
					outputStream.write(outputBytes);
					
		            inputStream.close();
		            outputStream.close();
		            
		            return 0;
				} catch (IOException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return -1;
				}
	             
	    }
}
